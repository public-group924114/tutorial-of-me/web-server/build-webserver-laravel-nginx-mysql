# install ubuntu
1. install ubuntu (i'm installing ubuntu version 22.10)
if any error run `sudo apt update`, you can another version
2. you can run `sudo apt update`
3. you can run `sudo apt-get install net-tools`
    #if you install ubuntu in virtual machine, you can remote server using ssh,
    1. check version ssh using `ssh -V`, if you havent installing ssh, you can following line sudo apt install openssh-server
4. you can runsudo apt install iputils-ping 
    #if vim not installed you can use sudo apt-get install vim
5. you can run `sudo add-apt-repository ppa:ondrej/php -y`
    if you want using multiple php and if your version ubuntu is version 22.04, file `ondrej-ubuntu-php-kinetic.list` any on directory `/etc/apt/sources.list.d`, 
    if any file ondrej-ubuntu-php-kinetic.list replace the content with 
    ```  
     deb https://ppa.launchpadcontent.net/ondrej/php/ubuntu/ jammy main 
   # deb https://ppa.launchpadcontent.net/ondrej/php/ubuntu/ kinetic main
   # deb-src https://ppa.launchpadcontent.net/ondrej/php/ubuntu/ kinetic main
   ```
if not error after update, run `sudo apt install php7.4` (if you want use version php7.4) 
or `sudo apt install php8.1` (if you want use php version 8), 
next if you want to check any version php you can run `sudo update-alternatives --config php`
and if u want use version php you can select number of order number, 
you need install package php for installing laravel you can use command 
 `sudo apt install php8.1-cli php8.1-fpm php8.1-common php8.1-zip php8.1-mbstring php8.1-curl php8.1-xml php8.1-bcmath`
6. After installing php, the next step is to install nginx `sudo apt install nginx`
7. After installing nginx, next step is to install composer 
```
    first step you can following this line :
    - curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
    Verify the hash of the downloaded PHP composer script with the signatures present at the official page:
    - HASH=`curl -sS https://composer.github.io/installer.sig`
    Then, validate if the PHP Composer installer can be safely executed or not:
    - php -r "if (hash_file('SHA384', '/tmp/composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    this command will be return " Installer verified "
    after check downloaded composer and validate you can run this following line :
    - sudo php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer
    if your composer success installed, you can check using following line :
    - composer -V
    (sources install composer https://linuxhint.com/install-and-use-php-composer-ubuntu-22-04/)
```
8. after install composer, please install ssl using following line :
```
    - sudo apt install opensll
    if any openssl on your ubuntu, you can check using command :
    - openssl version
```
9. next step install laravel "i'm using laravel 9" :
```
    go to folder /var/www and add permision using command `sudo chmod 777 -R .`  and then run following line :
    - composer create-project laravel/laravel:^9.0 trondol-racing
```
10. now generate ssl
```
    go to folder /etc/nginx/certificate if not there u can run :
    - sudo mkdir /etc/nginx/certificate
    after make directory, next step go to /etc/nginx/certificate and run 
    - sudo openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out nginx-certificate.crt -keyout nginx.key
```
11. ssl done installed, now created configuration nginx
```
    create configuration using command
    - sudo touch /etc/nginx/sites-available/trondolanracing.config
    copy  and change configuration in (https://gitlab.com/public-group924114/tutorial-of-me/assets/nginx/-/raw/main/sites-available/webconfig-php-7-4-and-SSL) and fill on your file config /etc/nginx/sites-available/trondolanracing.config
```
12. add symling configuration using command `sudo ln -s /etc/nginx/sites-available/trondolanracing.conf /etc/nginx/sites-enabled/`
13. check configuration nginx using command
```
    - sudo nginx -t
    if any error please restart nginx using command
    - sudo systemctl restart nginx
```
